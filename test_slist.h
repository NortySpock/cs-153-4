//////////////////////////////////////////////////////////////////////////////
/// @file test_slist.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_slist header file, which defines the unit tests
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @class test_slist
/// @brief This is the header file for the class that tests the Singly
/// linked list.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_slist);
/// @brief This calls the unit testing library for CPP, 
/// using this header file as an argument
/// @pre Requires the indicated header file to exist 
/// @post Reports the success or failure of unit tests.
/// @param Test_slist is the header being called
/// @return I believe this is void, instead cout-ing 
/// the results to the terminal
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_copy_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_copy_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_copy_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_assignment);
/// @brief This calls the unit testing library for CPP, 
/// using the test_assignment member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_assignment is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_push_front);
/// @brief This calls the unit testing library for CPP, 
/// using the test_push_front member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_push_front is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_pop_front);
/// @brief This calls the unit testing library for CPP, 
/// using the test_pop_front member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_pop_front is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_remove);
/// @brief This calls the unit testing library for CPP, 
/// using the test_remove member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_remove is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_front);
/// @brief This calls the unit testing library for CPP, 
/// using the test_front member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_front is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_clear);
/// @brief This calls the unit testing library for CPP, 
/// using the test_clear member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_clear is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_size);
/// @brief This calls the unit testing library for CPP, 
/// using the test_size member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_size is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_empty);
/// @brief This calls the unit testing library for CPP, 
/// using the test_empty member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_empty is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the SList class
/// @pre SList class should already be declared. 
/// This function will instantiate an SList and make sure the initial 
/// values are correct (size is zero, m_front is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the SList class
/// @pre SList class should already be declared. 
/// This function will instantiate an SList and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment operator of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_front ();
/// @brief This tests the push_front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_front ();
/// @brief This tests the pop_front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_remove ();
/// @brief This tests the remove function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_front ();
/// @brief This tests the front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_SLIST_H
#define TEST_SLIST_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "slist.h"


class Test_slist : public CPPUNIT_NS::TestFixture
{
  private:
	CPPUNIT_TEST_SUITE (Test_slist);
		CPPUNIT_TEST (test_constructor);
		CPPUNIT_TEST (test_copy_constructor);
		CPPUNIT_TEST (test_assignment);
		CPPUNIT_TEST (test_push_front);
		CPPUNIT_TEST (test_pop_front);
		CPPUNIT_TEST (test_remove);
		CPPUNIT_TEST (test_front);
		CPPUNIT_TEST (test_clear);
		CPPUNIT_TEST (test_size);
		CPPUNIT_TEST (test_empty);
	CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor();
  void test_copy_constructor();
  void test_assignment();
  void test_push_front();
  void test_pop_front();
  void test_remove();
  void test_front();
  void test_clear();
  void test_size();
  void test_empty();
};

#endif
