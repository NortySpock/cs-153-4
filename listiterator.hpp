//////////////////////////////////////////////////////////////////////////////
/// @file listiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the listiterator class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator ()
/// @brief Constructs the iterator
/// @pre No iterator exists
/// @post Iterator exists
/// @param None (constructor)
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
ListIterator<generic>::ListIterator () : m_current (NULL)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator (Node<generic> *)
/// @brief Copy constructor for iterator
/// @pre A previous iterator must exist.
/// @post Current iterator contains the same settings as the old iterator.
/// @param Existing iterator to be copied
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
ListIterator<generic>::ListIterator (Node<generic> * x) : m_current (x)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Returns value of contents at iterator
/// @pre Iterator and list must exist
/// @post No change
/// @param None
/// @return Returns value of data at current position of iterator
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
generic ListIterator<generic>::operator* () const
{
    return m_current->data;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator operator++ ();
/// @brief Moves iterator one step along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator one more step along list
/// @param None.
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
ListIterator<generic> ListIterator<generic>::operator++ ()
{
    m_current = m_current-> f;
    return *this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ (int);
/// @brief Moves iterator indicated number of steps along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator 'n' more steps along list
/// @param 'n' steps to take along list
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
ListIterator<generic> ListIterator<generic>::operator++ (int)
{
    return ++(*this);//I have no flying clue how this works
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool ListIterator<generic>::operator== (const ListIterator & rhs) const
{
	//RHS == right hand side
  return m_current == rhs.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool ListIterator<generic>::operator!= (const ListIterator & rhs) const
{
	//RHS == right hand side
    return m_current != rhs.m_current;
}


