//////////////////////////////////////////////////////////////////////////////
/// @file test_slist.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_slist implementation file
//////////////////////////////////////////////////////////////////////////////


#include "test_slist.h"
#include <iostream>
#include <cmath>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_slist);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the SList class
/// @pre SList class should already be declared. 
/// This function will instantiate an SList and make sure the initial 
/// values are correct (size is zero, m_front is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_constructor ()
{  
  //cout << endl << "Testing creation of SList...";
  SList <int> a;
  CPPUNIT_ASSERT(a.size () == 0);//size of list is zero
  CPPUNIT_ASSERT(a.empty() == true);//front is null -- it's empty
  //cout << endl << "Success!";
  //cout << endl;  
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the SList class
/// @pre SList class should already be declared. 
/// This function will instantiate an SList and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_copy_constructor()
{
  //cout << endl << "Prepping for copy constructor test...";
  const unsigned int TEST_MAX = 5000;
 
  //cout << endl << "Testing empty copy constructor...";
  SList <int> a;
  //Yes, I'm testing the empty case. So sue me.
  SList <int> b(a);
    
  SListIterator<int> u;//Create iterator
  SListIterator<int> v;//Create another iterator
  
  CPPUNIT_ASSERT (a.size() == b.size());
  
  for (u = a.begin (), v = b.begin (); u != a.end (); u++, v++)
  {
    CPPUNIT_ASSERT (*u == *v);
  }
  
  
  //Testing one element in list...
  //cout << endl << "Testing single copy constructor...";
  SList <int> c;
  c.push_front(9);//Push in one element
  
  SList <int> d(c);//copy
  
  SListIterator<int> w;//Create iterator
  SListIterator<int> x;//Create another iterator
  
  CPPUNIT_ASSERT (c.size() == d.size());
  
  for (w = c.begin (), x = d.begin (); w != c.end (); w++, x++)
  {
    CPPUNIT_ASSERT (*w == *x);
  }
  
  //And now for the big kahuna...
  //cout << endl << "Testing multiple element copy constructor...";
  SList <int> e;
  for (unsigned int i = 0; i < TEST_MAX; i++)
  {
    e.push_front(i);
  }
  
  SList <int> f(e);
  
  SListIterator<int> y;//Create iterator
  SListIterator<int> z;//Create another iterator
  
  CPPUNIT_ASSERT (e.size() == f.size());
  for(y = e.begin (), z = f.begin (); y != e.end (); y++, z++)
  {
    CPPUNIT_ASSERT (*y == *z);
  }
  
  //Copy constructor success!
  //cout << endl << "Success!";
  //cout << endl;  
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment operator of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_assignment()
{
  //cout << endl << "Prepping for assignment test...";
  const unsigned int TEST_MAX = 5000;
 
  //cout << endl << "Testing empty list assignment...";
  SList <int> a;
  //Yes, I'm testing the empty case. So sue me.
  SList <int> b;
  
  b = a;
    
  SListIterator<int> u;//Create iterator
  SListIterator<int> v;//Create another iterator
  
  CPPUNIT_ASSERT (a.size() == b.size());
  
  for (u = a.begin (), v = b.begin (); u != a.end (); u++, v++)
  {
    CPPUNIT_ASSERT (*u == *v);
  }
  
  
  //Testing one element in list...
  //cout << endl << "Testing single element SList assignment...";
  SList <int> c;
  c.push_front(9);//Push in one element
  
  SList <int> d;
  d = c;//copy
  
  SListIterator<int> w;//Create iterator
  SListIterator<int> x;//Create another iterator
  
  CPPUNIT_ASSERT (c.size() == d.size());
  
  for (w = c.begin (), x = d.begin (); w != c.end (); w++, x++)
  {
    CPPUNIT_ASSERT (*w == *x);
  }
  
  //And now for the big kahuna...
  //cout << endl << "Testing multiple element SList assignment...";
  SList <int> e;
  for (unsigned int i = 0; i < TEST_MAX; i++)
  {
    e.push_front(i);
  }
  
  SList <int> f;
  
  f = e;//And here I thought f=ma...
  
  SListIterator<int> y;//Create iterator
  SListIterator<int> z;//Create another iterator
  
  CPPUNIT_ASSERT (e.size() == f.size());
  for(y = e.begin (), z = f.begin (); y != e.end (); y++, z++)
  {
    CPPUNIT_ASSERT (*y == *z);
  }
  

  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_front ();
/// @brief This tests the push_front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_push_front()
{
  const unsigned int TEST_MAX = 5000;

  //cout << endl << "Prepping for push_front test...";
  SList <int> a;
  //cout << endl << "Testing push front...";

  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i*2);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT((a.front()) == (i*2));
  }

  //cout << endl << "Success!";
  //cout << endl;  
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_front ();
/// @brief This tests the pop_front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_pop_front()
{
	//cout << endl << "Prepping for pop front...";
	SList <int> a;
	const int TEST_MAX = 5000;
	
	for(int i = 0; i < TEST_MAX; i++)
	{
	  a.push_front(i);
	}
	
	
	//cout << endl << "Testing pop front...";
	for(int i = TEST_MAX; i > 0; i--)
	{
	  CPPUNIT_ASSERT(a.size() == i);
		CPPUNIT_ASSERT(a.front() == i-1);
		a.pop_front();
	}
		
	try//popping nothing
	{
		a.pop_front();
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
	}
	catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_remove ();
/// @brief This tests the remove function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_remove()
{
  //cout << endl << "Prepping for remove test...";
  const unsigned int TEST_MAX = 5000;
  
  
  SList <int> a;
  
   //cout << endl << "Testing empty list remove exception...";
  try//Try to remove from an empty list
  {
    a.remove(5);
	CPPUNIT_ASSERT(0);//If it hits this you screwed up.
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
 

 //cout << endl << "Further prepping for remove test...";
  //Fill the list
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
  }
  
  //Just making sure we ended where we should
  CPPUNIT_ASSERT (a.front() == 4999);
  
  //Remove last item in list
  //cout << endl << "Testing front remove...";
  a.remove(4999);
  
  CPPUNIT_ASSERT (a.front() == 4998);
  CPPUNIT_ASSERT (a.size() == TEST_MAX-1);
  
  
  
  //Remove item from middle of list
  //cout << endl << "Testing middle remove...";
  a.remove(2000);
  CPPUNIT_ASSERT (a.size() == TEST_MAX-2);
  
  //Remove item from outside of list -- should cause no change in size.
  //cout << endl << "Testing remove from item not in the SList...";
  try
  {
    a.remove(9000);
	CPPUNIT_ASSERT(0);//If you hit this, you screwed up.
  }
  catch(Exception & e)
  {
        CPPUNIT_ASSERT (ITEM_NOT_FOUND == e.error_code ());
		CPPUNIT_ASSERT (a.size() == TEST_MAX-2);
  }
  
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_front ();
/// @brief This tests the front function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_front()
{
 //cout << endl << "Prepping for front test...";
  const unsigned int TEST_MAX = 5000;
  
  SList <int> a;
 
 //cout << endl << "Testing front exception...";
 try
 { 
   a.front();
   CPPUNIT_ASSERT(0);
 }
 catch(Exception & e)
 {
   CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
 }
  
  //cout << endl << "Testing front...";   
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
		CPPUNIT_ASSERT (a.front() == i); 
  }
  
  //cout << endl << "Success!";
  //cout << endl;
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_clear()
{
  //cout << endl << "Prepping for clear test...";
  const unsigned int TEST_MAX = 5000;
  
  SList <int> a;
  
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
  }
  
  //cout << endl << "Testing clear...";   
  a.clear();//CLEAR!!
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_size()
{
  //cout << endl << "Prepping for size test...";//No jokes, please...
  const unsigned int TEST_MAX = 5000;
  
  SList <int> a;
  
  
  //cout << endl << "Testing size...";   
  CPPUNIT_ASSERT(a.size() == 0);
  
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
	a.push_front(i);
  } 
  
  a.clear();
  
  CPPUNIT_ASSERT(a.size() == 0);

  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the SList class
/// @pre SList class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_slist::test_empty()
{
  //cout << endl << "Prepping for empty test...";
  const unsigned int TEST_MAX = 5000;
  
  SList <int> a; 
  
  //cout << endl << "Testing empty...";   
  
  CPPUNIT_ASSERT (a.empty());
  
  a.push_front(4);
  
  CPPUNIT_ASSERT (!a.empty());
  
  a.clear();
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}
