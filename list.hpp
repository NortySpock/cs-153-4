//////////////////////////////////////////////////////////////////////////////
/// @file list.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the List class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn List ()
/// @brief This is the constructor for the List. It sets the initial size 
/// of the list to zero and points the head (m_front) and tail (m_back) 
/// at NULL.
/// @pre No List exists
/// @post An empty List of size 0 and with m_front and m_back pointing 
/// at NULL exists.
/// @param None (default constructor)
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
List<generic>::List()
{
	m_size = 0;
	m_front = NULL;
	m_back = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn List (const List <generic> &)
/// @brief This is the copy constructor. It copies a list (given as a 
/// parameter) directly into the new list. (Note: this is a deep copy)
/// @pre A List exists. 
/// @post A new List exists, an exact copy of the list passed as a parameter.
/// @param The List to be copied.
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
List<generic>::List (const List <generic> &dll)
{
  m_size = 0;
  m_front = NULL;
  m_back = NULL;
  
  *this = dll;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ~List ()
/// @brief This is the destructor for the List. It calls clear, which just
/// deletes everything in the list using pop_fronts.
/// @pre An List exists
/// @post No List exists (this assists in deleting it.)
/// @param None. (Destructor)
/// @return None. (Destructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
List<generic>::~List ()
{
  clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn List & operator= (const List &)
/// @brief The assignment operator definition for the List class. This puts 
/// everything in list to the right of the operator into the list on the left
/// of the operator. 
/// @pre Two linked lists exist.
/// @post Both lists now contain identical information.
/// @param The list to be set.
/// @param The list to be gotten.
/// @return The List, such that it is identical to the other SList.
////////////////////////////////////////////////////////////////////////////// 

template <class generic>
List<generic> & List<generic>:: operator= (const List <generic> &dll)
{
  if(dll.size() == 0)
  {
    clear();
  }
  else if(dll.size() == 1)
  {
    clear();//Clear the calling list.
		push_front(dll.front());
	
  }
  else//more than one
  {
		clear();//Clear the calling list.
	
		ListIterator<generic> j=dll.begin();//Create iterator
  
		//Starting with a pop front 
		//and then a for loop keeps the m_front from getting detached 
		//from the front of the list.
		push_front(dll.front()); //set first item
  
		Node<generic>* temp1 = m_front; //temp1 begins at m_front
		Node<generic>* temp2;
		
		//J begins at beginning plus one
		for( j++; j != dll.end(); j++)
		{
			temp2 = new Node<generic>; //Make a new node
			temp2 -> f = NULL; //Set the pointer on the new node to null
			temp2 -> data = *j; //Copy from old list into new node
			temp1 -> f = temp2; //Link one and two
	    temp1 = temp1 -> f;//Move forward one
			m_size++;
		}	
  }  
  return *this; //Return the resulting array.
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void push_front (generic)
/// @brief Pushes a new item onto the front of the list.
/// @pre List exists.
/// @post List contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void List<generic>::push_front (generic x)
{
  //If nothing in the array
  if(empty())
  {
    Node<generic>* temp; //Create new pointer
		temp = new Node<generic>; //Create a new node
		temp -> f = NULL; //Set forward pointer on node to null
		temp -> b = NULL; //Set backward pointer on node to null
		temp -> data = x; //Put the data into the node
		
		m_front = temp; //Point m_front at the new node
		m_back = temp; //Point m_back at the new node		
  }
  else//Something already in the array
  {
    Node<generic>* temp; //Create new pointer
		temp = new Node<generic>; //Create a new node with temp
		
		temp -> f = m_front; //Point forward at the front of the list
		temp -> b = NULL; //Point backward to null
		temp -> data = x; //Shove the data into the node
		
		temp -> f -> b = temp; //Point the next node back to this new one
		
		m_front = temp; //Point m_front at the new head
	}
  
	m_size++;//The list has one more item in it.
  
  return;//void
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_front ()
/// @brief This removes an item from the front of the list. 
/// @pre A List that contains at least one element.
/// @post The List contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void List<generic>::pop_front ()
{
  if(empty())//no items in list
  {
   throw Exception(CONTAINER_EMPTY, "There are no items in the list.");
  }
  else if(m_size == 1)//If we're removing the last node in the list
	{
		delete m_front; //delete the last item
		
		//Reset those pointers
		m_front = NULL; 
		m_back = NULL;
		
		m_size--;//m_size is now zero
	}
	else//There's at least two items in the list
	{
		Node<generic>* temp = m_front; //create temp and set temp equal to m_front
		m_front = m_front -> f; //Move m_front forward one.
	
		/* Set the backward pointer to null, as it's now front
		 * This would have segfaulted in the one-item list, so that's why
		 * that is a special case.
		 */
		m_front -> b = NULL; 
	
		delete temp; //Delete the item pointed at by temp (the old m_front
  
		m_size--;//Decrement size
	}
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_back(generic)
/// @brief Pushes a new item onto the back of the list.
/// @pre List exists.
/// @post List contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void List<generic>::push_back(generic x)
{
  //If nothing in the array
  if(empty())
  {
		/*Here, I call push_front for simplicity
		 *Note that this will require a small change, detailed below.
		 */
    push_front(x);
  }
  else//Something already in the array
  {
    Node<generic>* temp; //Create new pointer
		temp = new Node<generic>; //Create a new node with temp
		
		temp -> f = NULL; //Point forward to null, because we're at end of list
		temp -> b = m_back; //Point backward to tail
		temp -> data = x; //Shove the data into the node
		
		temp -> b -> f = temp; //Point the next node back to this new one
		
		m_back = temp; //Point m_front at the new head
		
		/*Notice that, here, I pull the increment operator into the else
		 *statement, because otherwise I would double increment because push_front
		 *already does an increment.
		 */
		m_size++;//The list has one more item in it.
	}
  
  return;//void
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_back ()
/// @brief This removes an item from the back of the list. 
/// @pre A List that contains at least one element.
/// @post The List contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void List<generic>::pop_back ()
{
  if(empty())//no items in list
  {
   throw Exception(CONTAINER_EMPTY, "There are no items in the list.");
  }
  else if(m_size == 1)//If we're removing the last node in the list
	{
		delete m_front; //delete the last item
		
		//Reset those pointers
		m_front = NULL; 
		m_back = NULL;
		
		
	}
	else//There's at least two items in the list
	{
		Node<generic>* temp = m_back; //create temp and set temp equal to m_back
		m_back = m_back -> b; //Move m_back, back one.
	
		/* Set the forward pointer to null, as it's now front
		 * This would have segfaulted in the one-item list, so that's why
		 * that is a special case.
		 */
		m_back -> f = NULL; 
	
		delete temp; //Delete the item pointed at by temp (the old m_front)
  
		
	}
	m_size--;//Decrement size
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief This removes one instance of the passed value from the list.
/// Note that if the passed value is not in the list, remove will throw an 
/// "ITEM_NOT_FOUND" exception.
/// @pre A linked list exists.
/// @post The linked list contains one less instance of the passed value.
/// Size is decremented accordingly.
/// @param Value to be removed
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void List<generic>::remove(generic x)
{
  if(empty())
  {
    throw Exception (CONTAINER_EMPTY, "The list has nothing in it.");
  }
  else if(front() == x)//x is the first item
  {
    pop_front();
  }
  else if(back() == x)//x is the first item
  {
    pop_back();
  }
  else//x should be out there, but is not first item
  {
    Node<generic>* temp1;
    Node<generic>* temp2;
  
		temp1 = m_front;
		while(temp1 -> f != NULL && temp1 -> f -> data != x)
		{
			temp1 = temp1 -> f;
		}
		if(temp1 -> f == NULL)//We hit the end of the list without finding the item
		{
			throw Exception (ITEM_NOT_FOUND, "The specified element was not found.");
		}
    else//we found it
		{
			temp2 = temp1 -> f -> f;
			delete temp1 -> f;
			temp1 -> f = temp2;
			m_size--;
			return;
		}
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic front () const
/// @brief Returns the value of the first item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the first item in the list.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic List<generic>::front () const
{ 
  if(empty())//No items in the list
  {
    throw Exception(CONTAINER_EMPTY, "There is no data to return.");
  }
  else
  {
    return (m_front -> data); //return the data from the first node
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic back () const
/// @brief Returns the value of the last item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the last item in the list.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic List<generic>::back () const
{ 
  if(empty())//No items in the list
  {
    throw Exception(CONTAINER_EMPTY, "There is no data to return.");
  }
  else
  {
    return (m_back -> data); //return the data from the last node
  }
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Empties the list completely by continuously calling pop_front until
/// the list is empty.
/// @pre List exists
/// @post List has nothing in it, and m_front points to null.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void List<generic>::clear ()
{
  while(!empty())//So long as we have something in the list
  {
    pop_front(); //remove an item
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty () const
/// @brief Returns whether or not the list is empty
/// @pre List exists
/// @post No change
/// @param None
/// @return Boolean telling if the list is empty or not.
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool List<generic>::empty () const
{
  if(m_front == NULL && m_back == NULL)//Both are null
  {
    return true;
  }
  else if(m_front != NULL && m_back != NULL)//Both not null
  {
    return false;
  }
	//There should be no way for this to happen. If I wrote this correctly.
	/*And yet, while debugging this program, I actually had this error thrown.
	 *And couting the message of this exception actually helped me. So Nyah.
	 */
	else//m_front and m_back state mismatch! Fatal Error!
	{
	  throw Exception(HEAD_TAIL_MISMATCH,
	  "The state of the head and tail of the list don't match! FATAL ERROR.");
	}
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size () const
/// @brief Returns the current number of elements in the list
/// @pre List exists
/// @post No change.
/// @param None.
/// @return Returns the current number of items in the array (0 - whatever.)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
unsigned int List<generic>::size () const
{
  return m_size;
}


template <class generic>
ListIterator<generic> List<generic>::begin () const
{
    return Iterator (m_front);
}

template <class generic>
ListIterator<generic> List<generic>::end () const
{
    return Iterator ();
}
