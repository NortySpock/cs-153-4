//////////////////////////////////////////////////////////////////////////////
/// @file queue.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the queue implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn Queue<generic>::Queue ()
/// @brief This is the constructor of the Queue class
/// @pre Queue does not exist
/// @post A queue exists (it inherits from the SList)
/// @param None.
/// @return None. (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Queue<generic>::Queue()
{}

//////////////////////////////////////////////////////////////////////////////
/// @fn Queue<generic>::Queue (Queue &x) : List<generic> (x)
/// @brief This is the copy constructor of the Queue class
/// @pre A Queue exists (I almost wrote sexist, but that would be wrong.)
/// @post Two queues exist, exact (deep) copies of each other
/// @param The queue to be copied.
/// @return None. (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Queue<generic>::Queue (Queue & x) : List <generic> (x)
{}

//////////////////////////////////////////////////////////////////////////////
/// @fn Queue<generic>::~Queue ()
/// @brief This is the destructor of the Queue class
/// @pre Queue exists
/// @post No queue exists
/// @param None.
/// @return None. (destructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Queue<generic>::~Queue ()
{
	clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool Queue<generic>::empty () const
/// @brief Returns whether or not the queue is currently empty
/// @pre Queue exists
/// @post No change
/// @param None.
/// @return Boolean: True if queue has no items in it, false if otherwise.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool Queue<generic>::empty () const
{
	return List<generic>::empty();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int Queue<generic>::size () const
/// @brief Returns the current number of items in the queue
/// @pre Queue exists
/// @post No change
/// @param None.
/// @return Current number of items in the queue
//////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Queue<generic>::size () const
{
	return List<generic>::size();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic front ()
/// @brief Returns the item currently at the front
/// @pre Queue exists
/// @post No change
/// @param None.
/// @return Value of item currently on the front of the queue
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic Queue<generic>::front ()
{
	return List<generic>::front();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic back ()
/// @brief Returns the item currently at the back
/// @pre Queue exists
/// @post No change
/// @param None.
/// @return Value of item currently on the back of the queue
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic Queue<generic>::back ()
{
	return List<generic>::back();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void Queue<generic>::push (generic x)
/// @brief Pushes an item onto the back end of the queue
/// @pre Queue exists
/// @post Queue has one more item in it
/// @param Item to be pushed
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Queue<generic>::push (generic x)
{
	List<generic>::push_back(x);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void Queue<generic>::pop()
/// @brief Pops an item off the front
/// @pre Queue exists
/// @post Queue has one less item in it
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Queue<generic>::pop ()
{
	List<generic>::pop_front();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void Queue<generic>::clear()
/// @brief Pops all the items off the queue
/// @pre Queue exists
/// @post Queue has no items in it.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Queue<generic>::clear ()
{
	List<generic>::clear();
}

