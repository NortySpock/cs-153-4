//////////////////////////////////////////////////////////////////////////////
/// @file stack.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the stack implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn Stack<generic>::Stack ()
/// @brief This is the constructor of the Stack class
/// @pre Stack does not exist
/// @post A stack exists (it inherits from the SList)
/// @param None.
/// @return None. (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Stack<generic>::Stack ()
{}

//////////////////////////////////////////////////////////////////////////////
/// @fn Stack<generic>::Stack (Stack &x) : SList<generic> (x)
/// @brief This is the copyconstructor of the Stack class
/// @pre A Stack exists (I almost wrote sexist, but that would be wrong.)
/// @post Two stacks exist, exact (deep and independent) copies of each other
/// @param The stack to be copied.
/// @return None. (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Stack<generic>::Stack (Stack &x) : SList<generic> (x)
{}

//////////////////////////////////////////////////////////////////////////////
/// @fn Stack<generic>::~Stack ()
/// @brief This is the destructor of the Stack class
/// @pre Stack exists
/// @post No stack exists
/// @param None.
/// @return None. (destructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
Stack<generic>::~Stack ()
{
	clear();
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void Stack<generic>::push (generic x)
/// @brief Pushes an item onto the top of the stack
/// @pre Stack exists
/// @post Stack has one more item in it
/// @param Item to be pushed
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Stack<generic>::push (generic x)
{
    SList<generic>::push_front(x);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void Stack<generic>::pop()
/// @brief Pops an item off the top 
/// @pre Stack exists
/// @post Stack has one less item in it
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Stack<generic>::pop()
{
    SList<generic>::pop_front();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void Stack<generic>::clear()
/// @brief Pops all the items off the stack
/// @pre Stack exists
/// @post Stack has no items in it.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void Stack<generic>::clear ()
{
	SList<generic>::clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic & Stack<generic>::top ()
/// @brief Returns the item currently on top.
/// @pre Stack exists
/// @post No change
/// @param None.
/// @return Value of item currently on the top of the stack
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic & Stack<generic>::top ()
{
	return SList<generic>::front();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int Stack<generic>::size () const
/// @brief Returns the current number of items in the stack
/// @pre Stack exists
/// @post No change
/// @param None.
/// @return Current number of items in the stack
//////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Stack<generic>::size () const
{
	return SList<generic>::size();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool Stack<generic>::empty () const
/// @brief Returns whether or not the stack is currently empty
/// @pre Stack exists
/// @post No change
/// @param None.
/// @return Boolean: True if stack has no items in it, false if otherwise.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool Stack<generic>::empty () const
{
	return SList<generic>::empty();
}

