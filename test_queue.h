//////////////////////////////////////////////////////////////////////////////
/// @file test_queue.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_queue header file, which defines the unit tests
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class test_queue
/// @brief This is the header file for the class that tests the Queue
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_queue);
/// @brief This calls the unit testing library for CPP, 
/// using this header file as an argument
/// @pre Requires the indicated header file to exist 
/// @post Reports the success or failure of unit tests.
/// @param Test_queue is the header being called
/// @return I believe this is void, instead cout-ing 
/// the results to the terminal
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_copy_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_copy_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_copy_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_assignment);
/// @brief This calls the unit testing library for CPP, 
/// using the test_assignment member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_assignment is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_push);
/// @brief This calls the unit testing library for CPP, 
/// using the test_push member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_push is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_pop);
/// @brief This calls the unit testing library for CPP, 
/// using the test_pop member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_pop is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_front);
/// @brief This calls the unit testing library for CPP, 
/// using the test_front member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_front is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_back);
/// @brief This calls the unit testing library for CPP, 
/// using the test_back member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_back is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_clear);
/// @brief This calls the unit testing library for CPP, 
/// using the test_clear member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_clear is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_size);
/// @brief This calls the unit testing library for CPP, 
/// using the test_size member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_size is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_empty);
/// @brief This calls the unit testing library for CPP, 
/// using the test_empty member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_empty is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the Queue class
/// @pre Queue class should already be declared. 
/// This function will instantiate a Queue and make sure the initial 
/// values are correct (size is zero, m_front is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the Queue class
/// @pre Queue class should already be declared. 
/// This function will instantiate an Queue and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment();
/// @brief This tests the assignment operator of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push();
/// @brief This tests the push function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop();
/// @brief This tests the pop function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_front ();
/// @brief This tests the top/front function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_back ();
/// @brief This tests the top/front function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_QUEUE_H
#define TEST_QUEUE_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "queue.h"


class Test_queue : public CPPUNIT_NS::TestFixture
{
  private:
	CPPUNIT_TEST_SUITE (Test_queue);
		CPPUNIT_TEST (test_constructor);
		CPPUNIT_TEST (test_copy_constructor);
		CPPUNIT_TEST (test_assignment);
		CPPUNIT_TEST (test_push);
		CPPUNIT_TEST (test_pop);
		CPPUNIT_TEST (test_front);
		CPPUNIT_TEST (test_back);
		CPPUNIT_TEST (test_clear);
		CPPUNIT_TEST (test_size);
		CPPUNIT_TEST (test_empty);
	CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor();
  void test_copy_constructor();
  void test_assignment();
  void test_push();
  void test_pop();
  void test_front();
  void test_back();
  void test_clear();
  void test_size();
  void test_empty();
};

#endif
